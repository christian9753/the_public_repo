node {
  stage("Clone repository") {
    git "https://gitlab.com/christian9753/python-movies.git"
  }
  
  stage("Lint") {
    sh "pwd"
    sh "ls -l"
    sh "pylint movie.py --output-format=html > output.html"
  }
//  stage("Report") {
//    publishHTML([
//      allowMissing: false,
//      alwaysLinkToLastBuild: false,
//      keepAll: false,
//      reportDir: '',
//      reportFiles: 'output.html',
//      reportName: 'Pylint Report'
//    ])
//  }
}
