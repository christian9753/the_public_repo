class Mammal(object):
  def breath(self):
    print "inhalt and exhale"

class Cat(Mammal):
  def speak(self):
    print "meow"

henry = Cat()
henry.breath()
henry.speak()